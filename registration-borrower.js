/// <reference types="cypress" />

// Welcome to Cypress!
//
// This spec file contains a variety of sample tests
// for a todo list app that are designed to demonstrate
// the power of writing tests in Cypress.
//
// To learn more about how Cypress works and
// what makes it such an awesome testing tool,
// please read our getting started guide:
// https://on.cypress.io/introduction-to-cypress

// const uuid = () => Cypress._.random(1e8, 1e9)
// const id = uuid()
// const testname = `testname${id}`

const randomNum = function randomId () {
  return Math.random()
    .toString()
    .substr(2, 10)
}
const randomnumber = randomNum()

function randomText() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

  for (var i = 0; i < 10; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}
const randomemail = randomText()
const randomusername = randomText()
const randompassword = randomText()

describe('login function', () => {
    beforeEach(() => {
      cy.visit('https://investree.tech/login', {
          headers: {
              authorization : 'Basic aW52ZXN0cmVlOmludmVzdG1lIQ=='
          }
      })
    })
  
    it('regist', () => {
      
      cy.get(':nth-child(4) > :nth-child(8)').click()
      cy.get('[data-unq="btn-beneficial-owner-0"]').click()
      cy.get('.row > .btn').click()
      cy.get(':nth-child(2) > .form-group > .form-control').type('please')
      cy.get(':nth-child(4) > .form-group > .form-control').type(randomemail+'kill@mee.com')
      cy.get('.input-group > .form-control').type(randomnumber)
      cy.get(':nth-child(7) > .form-group > .form-control').type(randomusername)
      cy.get('.password-field > .form-control').type(randompassword+randomnumber)
      cy.get(':nth-child(2) > .form-group > .checkbox').click()
      cy.get(':nth-child(3) > .form-group > .checkbox').click()
      cy.get(':nth-child(4) > .btn').click()
      cy.wait(10000)
      // cy.get('#input-0').type('1')
      // cy.get('#input-1').type('2')
      // cy.get('#input-2').type('3')
      // cy.get('#input-3').type('4')
      // cy.get('#input-4').type('5')
      // cy.get('#input-5').type('6')
      
    })

    it('with correct credential should succeed', () => {
      
        cy.get(':nth-child(1) > .form-group > .form-control').type(randomemail+'kill@mee.com')
        cy.get('.password-field > .form-control').type(randompassword+randomnumber)
        cy.get('#gtm-button-login').click()
        cy.wait(5000)
        
      })
    
    it('with wrong credential should fail', () => {
      
        cy.get(':nth-child(1) > .form-group > .form-control').type(randomemail+'kill@mee.com')
        cy.get('.password-field > .form-control').type('killme')
        cy.get('#gtm-button-login').click()
        
        
      })
  

  })
  